import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:lottie/lottie.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';
import 'package:sampe_introduction_widget/utils/size_config.dart';
import 'package:sampe_introduction_widget/views/home.dart';

class IntroScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // Page decoration
    PageDecoration pageDecoration = PageDecoration(
      titleTextStyle: TextStyle(
        fontSize: 28.0,
        fontWeight: FontWeight.w700,
        color: Colors.black,
      ),
      bodyTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 19.0,
      ),
      imagePadding: EdgeInsets.all(20),
      // boxDecoration: BoxDecoration(
      //   gradient: LinearGradient(
      //     begin: Alignment.topRight,
      //     end: Alignment.topLeft,
      //     stops: [0.1, 0.5, 0.7, 0.9],
      //     colors: [
      //       Colors.orange,
      //       Colors.deepOrangeAccent,
      //       Colors.red,
      //       Colors.redAccent,
      //     ],
      //   ),
      // ),
    );
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          middleCurve(context),
          // Container(
          //   height: MediaQuery.of(context).size.height,
          //   width: MediaQuery.of(context).size.width,
          //   decoration: BoxDecoration(
          //     gradient: LinearGradient(
          //       begin: Alignment.topRight,
          //       end: Alignment.topLeft,
          //       stops: [0.1, 0.5, 0.7, 0.9],
          //       colors: [
          //         Colors.orange,
          //         Colors.deepOrangeAccent,
          //         Colors.red,
          //         Colors.redAccent,
          //       ],
          //     ),
          //   ),
          // ),
          IntroductionScreen(
            globalBackgroundColor: Colors.transparent,
            pages: [
              PageViewModel(
                title: "Fractional shares",
                body:
                    "Instead of having to buy an entire share, invest any amount you want.",
                image: introImage("assets/images/forgot_password.png"),
                decoration: pageDecoration,
              ),
              PageViewModel(
                title: "Fractional shares",
                body:
                    "Instead of having to buy an entire share, invest any amount you want.",
                image: introImage("assets/images/sponsbob.png"),
                decoration: pageDecoration,
              ),
              PageViewModel(
                title: "Fractional shares @@@@",
                body:
                    "Instead of having to buy an entire share, invest any amount you want MEEEE.",
                image: introLottie("assets/animations/news.json"),
                decoration: pageDecoration,
              ),
            ],
            onDone: () => goHomepage(context),
            onSkip: () => goHomepage(context),
            showSkipButton: true,
            skipFlex: 0,
            nextFlex: 0,
            skip: Text(
              "Skip",
              style: TextStyle(color: Colors.black),
            ),
            next: Icon(
              Icons.arrow_forward_ios,
              color: Colors.black,
            ),
            done: Text(
              "Getting Started",
              style:
                  TextStyle(fontWeight: FontWeight.w600, color: Colors.black),
            ),
            dotsDecorator: DotsDecorator(
              size: Size(10.0, 10.0), //size of dots
              color: Colors.black, //color of dots
              activeSize: Size(22.0, 10.0),
              activeShape: RoundedRectangleBorder(
                //shave of active dot
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // navigation to Home
  void goHomepage(context) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) {
      return HomePage();
    }), (Route<dynamic> route) => false);
    //Navigate to home page and remove the intro screen history
    //so that "Back" button wont work.
  }

  // widget image
  Widget introImage(String assetName) {
    //widget to show intro image
    return Align(
      child: Image.asset('$assetName', width: 350.0),
      alignment: Alignment.bottomCenter,
    );
  }

  // widget lottie
  Widget introLottie(String assetName) {
    //widget to show intro image
    return Align(
      child: Lottie.asset('$assetName', width: 350.0, repeat: false),
      alignment: Alignment.bottomCenter,
    );
  }

  Widget middleCurve(BuildContext context) {
    return ClipPath(
      clipper: ProsteBezierCurve(
        position: ClipPosition.bottom,
        reclip: false,
        list: [
          BezierCurveSection(
            // start: const Offset(0, 150),
            start: Offset(0, SizeConfig.safeBlockVertical * 40),
            // top: Offset(MediaQuery.of(context).size.width / 4, 100),
            top: Offset(
              MediaQuery.of(context).size.width / 4,
              SizeConfig.safeBlockVertical * 20,
            ),
            // end: Offset(MediaQuery.of(context).size.width / 2, 125),
            end: Offset(
              MediaQuery.of(context).size.width / 2,
              SizeConfig.safeBlockVertical * 18,
            ),
          ),
          BezierCurveSection(
            // start: Offset(MediaQuery.of(context).size.width / 2, 125),
            start: Offset(
              MediaQuery.of(context).size.width / 2,
              SizeConfig.safeBlockVertical * 35,
            ),
            // top: Offset(MediaQuery.of(context).size.width / 4 * 3, 150),
            top: Offset(
              MediaQuery.of(context).size.width / 4 * 3,
              SizeConfig.safeBlockVertical * 25,
            ),
            // end: Offset(MediaQuery.of(context).size.width, 125),
            end: Offset(
              MediaQuery.of(context).size.width,
              SizeConfig.safeBlockVertical * 15,
            ),
          ),
        ],
      ),
      child: Container(
        height: SizeConfig.blockSizeVertical * 22,
        color: Colors.blue.shade400,
      ),
    );
  }
}
